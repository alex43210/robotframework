import json
from sqlalchemy import Column, Integer, String, Text
from .base import Model, Base


class Language(Model, Base):
    """
    Language common data
    """
    __tablename__ = "languages"
    id = Column(Integer, primary_key=True)
    codename = Column(String)
    stopwords_whitelist_text = Column(Text)

    @property
    def stopwords_whitelist(self):
        """
        Get stopwords "whitelist". E.g. - [["turn", "on"], ["turn", "off"]]
        :return: stopwords whiltelist
        :rtype: list[list[str]]
        """
        return json.loads(self.stopwords_whitelist_text)

    @stopwords_whitelist.setter
    def stopwords_whitelist(self, value):
        """
        Set stopwords "whitelist". E.g. - [["turn", "on"], ["turn", "off"]]
        :param value: stopwords whiltelist
        :type value: list[list[str]]
        """
        self.stopwords_whitelist_text = json.dumps(value, ensure_ascii=False)

    def __str__(self):
        return self.codename

















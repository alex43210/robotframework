from pynlc import TextProcessor
from sqlalchemy import Column, Integer, String, ForeignKey
from .base import Base, Model
from .language import Language
from .word_vectors import WordVectors


class Domain(Model, Base):
    """
    Domain-specific "language" data
    """
    __tablename__ = "domains"
    id = Column(Integer, primary_key=True)
    word_vectors_id = Column(ForeignKey("word_vectors.id"))
    domain = Column(String)
    language_id = Column(ForeignKey('languages.id'))

    def __str__(self):
        return "{0} - {1}".format(
            str(self.language), str(self.domain)
        )

    @property
    def word_vectors(self):
        """
        Get word vectors model object
        :return: word vectors
        :rtype: WordVectors
        """
        return WordVectors.filter(
            lambda query: query.where(WordVectors.id == self.word_vectors_id)
        )[0]

    @word_vectors.setter
    def word_vectors(self, vectors):
        """
        Set word vectors model object
        :param vectors: word vectors
        :type vectors: WordVectors
        """
        self.word_vectors_id = vectors.id

    @property
    def language(self):
        """
        Get language model object
        :return: language
        :rtype: Language
        """
        return Language.filter(
            lambda query: query.where(Language.id == self.language_id)
        )[0]

    @language.setter
    def language(self, language):
        """
        Set language model object
        :param language: language
        :type language: Language
        """
        self.language_id = language.id

    @property
    def instance(self):
        """
        Get text processor instance
        :return: text processor
        :rtype: TextProcessor
        """
        language = self.language
        word2vec = self.word_vectors.instance
        return TextProcessor(str(language.codename), language.stopwords_whitelist, word2vec)

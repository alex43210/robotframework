from collections import OrderedDict
from threading import Lock
from gensim.models import Word2Vec
from sqlalchemy import Column, Integer, String
from .base import Model, Base
from ..app import Application


class WordVectors(Model, Base):
    """
    Word vectors model
    """
    __tablename__ = "word_vectors"
    _instances = OrderedDict()
    _instances_lock = Lock()
    id = Column(Integer, primary_key=True)
    word2vec = Column(String)

    def __str__(self):
        return str(self.word2vec)

    def _reorder(self):
        path = str(self)
        with self._instances_lock:
            instance = self._instances[path]
            del self._instances[path]
            self._instances[path] = instance
        return instance

    @property
    def instance(self):
        """
        Get instance
        :return: Word2Vec instance
        :rtype: Word2Vec
        """
        path = str(self)
        if path in WordVectors._instances:
            return self._reorder()
        with WordVectors._instances_lock:
            if path in WordVectors._instances:
                return self._instances[path]
            WordVectors._instances[path] = Word2Vec.load_word2vec_format(path)
            paths = list(WordVectors._instances.keys())
            if len(paths) > Application().configuration['KEEP_WORD_VECTORS']:
                del WordVectors._instances[paths[0]]
        return self._reorder()

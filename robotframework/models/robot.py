from collections import OrderedDict
import json
import os
from threading import Lock
from robot import Robot as RobotBackend, Script
from sqlalchemy import update
from sqlalchemy import Column, Integer, String, Text, ForeignKey
from .base import Base, Model
from .domain import Domain
from .user import User
from ..app import Application


class Robot(Model, Base):
    """
    Robot model
    """
    __tablename__ = "robots"
    _instances = OrderedDict()
    _instances_lock = Lock()
    id = Column(Integer, primary_key=True)
    name = Column(String)
    domain_id = Column(ForeignKey("domains.id"))
    code = Column(Text)
    configuration_path = Column(Text, nullable=True, default=None)

    def _generate_configuration_path(self):
        base_path = Application().configuration['ROBOT_CONFIGURATIONS']
        return os.path.join(base_path, "{0}.json".format(self.id))

    @property
    def configuration(self):
        """
        Load configuration or return none
        :return: configuration or none
        :rtype: dict|NoneType
        """
        if not self.configuration_path:
            return None
        else:
            with open(self.configuration_path, "r", encoding="utf-8") as source:
                return json.load(source)

    @configuration.setter
    def configuration(self, value):
        """
        Set configuration
        :param value: configuration
        :type value: dict
        """
        if not self.configuration_path:
            self.configuration_path = self._generate_configuration_path()
        with open(self.configuration_path, "w", encoding="utf-8") as output:
            json.dump(value, output, ensure_ascii=False)

    @property
    def domain(self):
        """
        Get domain model
        :return: domain model
        :rtype: Domain
        """
        return Domain.filter(
            lambda query: query.where(Domain.id == self.domain_id)
        )[0]

    @domain.setter
    def domain(self, value):
        """
        Set domain model
        :param value: domain model
        :type value: Domain
        """
        self.domain_id = value.id

    @property
    def script_instance(self):
        """
        Get script instance
        :return: script
        :rtype: Script
        """
        return Script(str(self.code))

    def _reorder(self):
        with Robot._instances_lock:
            instance = Robot._instances[self.id]
            del Robot._instances[self.id]
            Robot._instances[self.id] = instance
        return instance

    @property
    def instance(self):
        """
        Get robot instance
        :return: robot
        :rtype: RobotBackend
        """
        if self.id in Robot._instances:
            return self._reorder()
        with Robot._instances_lock:
            if self.id in Robot._instances:
                return Robot._instances[self.id]
            app = Application()
            robot = RobotBackend(self.script_instance, self.domain.instance,
                                 app.configuration['OUTPUT_HANDLERS'],
                                 self.configuration)
            if self.configuration != robot.config:
                self.configuration = robot.config
                query = update(Robot).where(Robot.id == self.id).values({
                    "configuration_path": self.configuration_path
                })
                app.connection.execute(query)
            Robot._instances[self.id] = robot
            ids = list(Robot._instances.keys())
            if len(ids) >= app.configuration['KEEP_ROBOTS']:
                del Robot._instances[ids[0]]
        return self._reorder()

    def converse(self, user):
        """
        Initialize conversation
        :param user: user
        :type user: User
        :return: conversation
        :rtype: Conversation
        """
        from .conversation import Conversation
        app = Application()
        conversation = Conversation()
        conversation.robot = self
        conversation.user = user
        app.session.add(conversation)
        app.session.commit()
        return conversation

import json
from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey
from .base import Model, Base
from .conversation import Conversation


class ConversationItem(Model, Base):
    """
    Conversation item
    """
    TYPE_ROBOT = 0
    TYPE_HUMAN = 1

    __tablename__ = "conversation_items"
    id = Column(Integer, primary_key=True)
    conversation_id = Column(ForeignKey("conversations.id"))
    time = Column(DateTime(timezone=True))
    stage = Column(String)
    state_text = Column(Text)
    type = Column(Integer)
    text = Column(Text)

    @property
    def state(self):
        """
        Get state variables
        :return: state
        :rtype: dict[str, list[str]]
        """
        return json.loads(self.state_text)

    @state.setter
    def state(self, value):
        """
        Set state variables
        :param value: state
        :type value: dict[str, list[str]]
        """
        self.state_text = json.dumps(value, ensure_ascii=False)

    @property
    def conversation(self):
        """
        Get conversation
        :return: conversation
        :rtype: Conversation
        """
        return Conversation.filter(
            lambda query: query.where(Conversation.id == self.conversation_id)
        )[0]

    @conversation.setter
    def conversation(self, value):
        """
        Set conversation
        :param value: conversation
        :type value: Conversation
        """
        self.conversation_id = value.id

    def __str__(self):
        return self.text

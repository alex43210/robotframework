defaults = {
    'DATABASE_URI': '',
    'KEEP_WORD_VECTORS': 3,
    'ROBOT_CONFIGURATIONS': 'robot_configurations',
    'KEEP_ROBOTS': 5,
    'OUTPUT_HANDLERS': {}
}

from copy import deepcopy
import os
from threading import Lock
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .config import defaults


class Application(object):
    """
    Robot framework "application" object.
    """
    _instance = None          # Application is a singletone.
    _instance_lock = Lock()
    _initialized = False
    _initialized_lock = Lock()

    def __init__(self, configuration=None):
        """
        Initialize application
        :param configuration: configuration
        :type configuration: dict|NoneType
        """
        from .models import Base
        if Application._initialized:
            return
        with Application._initialized_lock:
            if Application._initialized:
                return
            Application._initialized = True
            self.configuration = self._build_configuration(configuration)
            self._check_configuration()
            self.database = create_engine(self.configuration['DATABASE_URI'])
            self.connection = self.database.connect()
            self.session = sessionmaker(bind=self.database)()
            Base.metadata.create_all(self.database)

    def _build_configuration(self, configuration):
        if configuration is None:
            configuration = {}
        full_configuration = deepcopy(defaults)
        for key, value in configuration.items():
            full_configuration[key] = value
        return full_configuration

    def _check_configuration(self):
        assert 'DATABASE_URI' in self.configuration and \
            isinstance(self.configuration['DATABASE_URI'], str) and \
            self.configuration['DATABASE_URI'] != ""
        assert 'ROBOT_CONFIGURATIONS' in self.configuration and \
            isinstance(self.configuration['ROBOT_CONFIGURATIONS'], str) and \
            os.path.isdir(self.configuration['ROBOT_CONFIGURATIONS'])
        assert 'KEEP_WORD_VECTORS' in self.configuration and \
            isinstance(self.configuration['KEEP_WORD_VECTORS'], int) and \
            self.configuration['KEEP_WORD_VECTORS'] > 0
        assert 'KEEP_ROBOTS' in self.configuration and \
               isinstance(self.configuration['KEEP_ROBOTS'], int) and \
               self.configuration['KEEP_WORD_VECTORS'] > 0
        assert 'OUTPUT_HANDLERS' in self.configuration and \
               isinstance(self.configuration['OUTPUT_HANDLERS'], list)
        for handler in self.configuration['OUTPUT_HANDLERS']:
            assert callable(handler)

    def __new__(cls, *args, **kwargs):
        if Application._instance is None:
            with Application._instance_lock:
                if Application._instance is None:
                    Application._instance = object.__new__(cls)
        return Application._instance

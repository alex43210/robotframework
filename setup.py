from setuptools import setup, find_packages


readme = 'NLC-based chatbot "wrapper"'

setup(
    name='robotframework',
    version='0.1',
    description=readme,
    author='Alexander Pozharskii',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
    ],
    packages=find_packages(),
    install_requires=['robot', 'sqlalchemy']
)

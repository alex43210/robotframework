from datetime import datetime
from robot import RobotState
from sqlalchemy import Column, Integer, ForeignKey
from .base import Model, Base
from .robot import Robot
from .user import User
from ..app import Application


class Conversation(Model, Base):
    """
    Conversation between user and robot.
    """

    __tablename__ = "conversations"
    id = Column(Integer, primary_key=True)
    robot_id = Column(ForeignKey("robots.id"))
    user_id = Column(ForeignKey("users.id"))

    @property
    def robot(self):
        """
        Get robot model object
        :return: robot
        :rtype: Robot
        """
        return Robot.filter(
            lambda query: query.where(Robot.id == self.robot_id)
        )[0]

    @robot.setter
    def robot(self, value):
        """
        Set robot model object
        :param value: robot
        :type value: Robot
        """
        self.robot_id = value.id

    @property
    def user(self):
        """
        Get user model object
        :return: user
        :rtype: User
        """
        return User.filter(
            lambda query: query.where(User.id == self.user_id)
        )[0]

    @user.setter
    def user(self, value):
        """
        Set user model object
        :param value: user
        :type value: User
        """
        self.user_id = value.id

    @property
    def items(self):
        """
        Get conversation items
        :return: items list
        :rtype: list[ConversationItem]
        """
        from .conversation_item import ConversationItem
        return ConversationItem.filter(
            lambda query: query.where(ConversationItem.conversation_id == self.id)\
                .order_by(ConversationItem.time).order_by(ConversationItem.type)
        )

    @property
    def state(self):
        """
        Get robot state (from last conversation item)
        :return: state
        :rtype: RobotState
        """
        items = self.items
        if len(items) == 0:
            return RobotState()
        else:
            last = items[-1]
            return RobotState(last.stage, last.state)

    def _new_item(self):
        """
        Initialize new conversation item
        :return: item
        :rtype: ConversationItem
        """
        from .conversation_item import ConversationItem
        result = ConversationItem()
        result.conversation = self
        result.time = datetime.now()
        return result

    def output(self):
        """
        Get robot output (by their state)
        :return: output
        :rtype: ConversationItem
        """
        from .conversation_item import ConversationItem
        app = Application()
        robot = self.robot.instance
        new_state, text = robot.output(self.state)
        item = self._new_item()
        item.state = new_state.variables
        item.stage = new_state.stage
        item.type = ConversationItem.TYPE_ROBOT
        item.text = text
        app.session.add(item)
        app.session.commit()
        return item

    def answer(self, text):
        """
        Get answer to user request
        :param text: request text
        :type text: str
        :return: answer
        :rtype: ConversationItem
        """
        app = Application()
        from .conversation_item import ConversationItem
        current_state = self.state
        input_item = self._new_item()
        input_item.state = current_state.variables
        input_item.stage = current_state.stage
        input_item.type = ConversationItem.TYPE_HUMAN
        input_item.text = text
        app.session.add(input_item)
        app.session.commit()
        if not self.state.stage:
            new_state = None
        else:
            robot = self.robot.instance
            new_state, text = robot.answer(self.state, text)
        output_item = self._new_item()
        if new_state is None:
            output_item.stage = None
            output_item.state = input_item.state
        else:
            output_item.stage = new_state.stage
            output_item.state = new_state.variables
        output_item.type = ConversationItem.TYPE_ROBOT
        output_item.text = text
        app.session.add(output_item)
        app.session.commit()
        return output_item

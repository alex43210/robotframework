Robotframework
--------------
Python library (depends from my [robot](https://bitbucket.org/alex43210/robot) and sqlalchemy),
that allows you to make chatbots with storing robots/conversations in databse and call it from your custom wrappers (e.g. - telegram bots).

Example
```
from robotframework import *
from sqlalchemy import select

app = Application({
    'KEEP_WORD_VECTORS': 4
})
robot = Robot.filter(lambda query: query.where(Robot.id == 1))[0]
user = User.filter(lambda query: query.where(User.id == 1))[0]
conversation = robot.converse(user)
#print(robot.instance)
items = lambda: print([str(item) for item in conversation.items])
items()
conversation.output()
conversation.answer('How hot it\'ll be in Moscow today?')
conversation.answer('Okay, turn off')
items()
```
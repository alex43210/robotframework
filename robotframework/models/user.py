import json
from sqlalchemy import Column, Integer, String, Text
from .base import Base, Model


class User(Model, Base):
    """
    User object model
    """
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    extra_text = Column(Text)

    @property
    def extra(self):
        """
        Get extra information
        :return: extra information
        :rtype: dict
        """
        return json.loads(self.extra_text)

    @extra.setter
    def extra(self, value):
        """
        Set extra information
        :param value: extra information
        :type value: dict
        """
        self.extra_text = json.dumps(value, ensure_ascii=False)

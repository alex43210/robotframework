from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import select
from ..app import Application


Base = declarative_base()


class Model:
    """
    Model class (use with multiparent inheritance with Base)
    """
    @classmethod
    def filter(cls, filter, operations=None):
        """
        Filter model objects by given filter
        :param filter: filter function
        :type filter: (sqlalchemy.sql.Select)->sqlalchemy.sql.Select
        :param operations: "operation" function (e.g. limit, order_by)
        :type operations: (sqlalchemy.sql.Select)->sqlalchemy.sql.Select|NoneType
        :return: filtered objects
        :rtype: list
        """
        if operations is None:
            operations = lambda q: q
        query = operations(filter(select([cls])))
        result = [cls(**item) for item in Application().connection.execute(query)]
        return result
